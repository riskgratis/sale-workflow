This module extends the functionality of the creation of a down payment for a sale order
and to allow you to set a tax on the down payment simultaneously
on the invoice line and the sale order line.
